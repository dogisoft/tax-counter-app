import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs/Observable";
import { CurrencyPipe } from '@angular/common';
import * as _ from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
  })

export class AppComponent implements OnInit {

	net: number = 0;
	gross: number = 0;
	vat: number = 0;
	vatrate: number = 0;
	vatrates = ['10', '13', '20'];
  isGrossDisabled : boolean = false;
  isNetDisabled : boolean = false;
  isVatDisabled : boolean = false;

  constructor() {}

  ngOnInit() { }

  resetForm(){
    this.isNetDisabled = false;
    this.isVatDisabled = false;
    this.net = 0;
    this.vat = 0;
    this.gross = 0;
    this.vatrate = 10;
  }

	getValue(actualItem){
		this.calculation(this.net, this.gross, this.vat, this.vatrate, actualItem);
	}

	protected calculation(net, gross, vat, vatrate, actualItem){

    if(this.gross > 0){
      this.isNetDisabled = true;
      this.isVatDisabled = true;
      this.net = (gross-(gross*vatrate)/100);
      this.vat = (this.gross-this.net);
    }
    else{
      if(this.net > 0){
        this.isGrossDisabled = true;
        this.isVatDisabled = true;
        this.gross = (100*net)/(100-vatrate);
        this.vat = (this.gross-this.net);
      }
      else{
        if(this.vat > 0){
          this.isGrossDisabled = true;
          this.isNetDisabled = true;
          this.gross = (100*vat)/10;
          this.net = (this.gross-this.vat);
        }
      }
    }
	}

}
